module bitbucket.org/labpatoscedro/uuid

go 1.15

require (
	github.com/Microsoft/go-winio v0.4.15 // indirect
	github.com/cenkalti/backoff/v3 v3.2.2 // indirect
	github.com/containerd/continuity v0.0.0-20200928162600-f2cc35102c2a // indirect
	github.com/moby/term v0.0.0-20201110203204-bea5bbe245bf // indirect
	github.com/ory/dockertest/v3 v3.6.2
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.7.0 // indirect
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.4.6
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
	golang.org/x/sys v0.0.0-20201117222635-ba5294a509c7 // indirect
)
