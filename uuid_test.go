package uuid_test

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"testing"

	. "bitbucket.org/labpatoscedro/uuid"
	"github.com/ory/dockertest/v3"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/bsontype"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type SampleObject struct {
	String string
	Custom UUID
}

var mongoClient *mongo.Client

func TestMain(m *testing.M) {
	ctx := context.Background()

	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("could not connect to docker: %s", err)
	}

	resource, err := pool.Run("mongo", "4", nil)
	if err != nil {
		log.Fatalf("could not start resource: %s", err)
	}

	if err := pool.Retry(func() error {
		var err error
		mongoClient, err = mongo.Connect(ctx, options.Client().ApplyURI(
			fmt.Sprintf("mongodb://localhost:%s", resource.GetPort("27017/tcp"))))
		if err != nil {
			return err
		}

		return mongoClient.Ping(ctx, readpref.Primary())
	}); err != nil {
		log.Fatalf("could not connect to docker resource: %s", err)
	}

	code := m.Run()

	if err := pool.Purge(resource); err != nil {
		log.Fatalf("could not purge resource: %s", err)
	}

	os.Exit(code)
}

func TestUUIDMarshaling(t *testing.T) {
	uuid := New()

	bsonType, bytes, err := uuid.MarshalBSONValue()
	assert.Nil(t, err)

	unmarshalError := uuid.UnmarshalBSONValue(bsonType, bytes)
	assert.Nil(t, unmarshalError)
}

func TestUUID_UnmarshalBSONValue(t *testing.T) {
	uuid := New()
	ctx := context.Background()

	_, bytes, err := uuid.MarshalBSONValue()
	assert.Nil(t, err)

	err = uuid.UnmarshalBSONValue(bsontype.Array, bytes)
	assert.Error(t, err, "invalid format on unmarshall bson value")

	err = uuid.UnmarshalBSONValue(bsontype.Binary, []byte(`1234`))
	assert.Error(t, err, "not enough bytes to unmarshal bson value")

	customUUID, err := FromString("53deef59-45e5-44a6-a015-77052ca37f39")
	assert.Nil(t, err)

	collection := mongoClient.Database("test").Collection("test")
	sample := &SampleObject{
		String: "53deef59-45e5-44a6-a015-77052ca37f39",
		Custom: customUUID,
	}

	_, err = collection.InsertOne(ctx, sample)
	assert.Nil(t, err)

	var dstInstance SampleObject
	err = collection.FindOne(ctx, map[string]interface{}{}).Decode(&dstInstance)
	assert.Nil(t, err)
	assert.Equal(t, sample.String, dstInstance.String)
	assert.Equal(t, sample.Custom, dstInstance.Custom)

	uuid = New()
	_, err = collection.InsertOne(ctx, map[string]interface{}{"custom": uuid.String()})
	assert.Nil(t, err)

	dstInstance = SampleObject{}
	err = collection.FindOne(ctx, map[string]interface{}{"custom": uuid.String()}).Decode(&dstInstance)
	assert.Nil(t, err)
	assert.Equal(t, uuid, dstInstance.Custom)

	uuid = New()
	_, err = collection.InsertOne(ctx, map[string]interface{}{"custom": nil})
	assert.Nil(t, err)

	dstInstance = SampleObject{}
	err = collection.FindOne(ctx, map[string]interface{}{"custom": nil}).Decode(&dstInstance)
	assert.Nil(t, err)
	assert.Equal(t, Nil, dstInstance.Custom)

	uuid = New()
	_, err = collection.InsertOne(ctx, map[string]interface{}{"custom": "invalid-string-id"})
	assert.Nil(t, err)

	dstInstance = SampleObject{}
	err = collection.FindOne(ctx, map[string]interface{}{"custom": "invalid-string-id"}).Decode(&dstInstance)
	assert.NotNil(t, err)

	_, err = collection.InsertOne(ctx, map[string]interface{}{"custom": ""})
	assert.Nil(t, err)

	dstInstance = SampleObject{}
	err = collection.FindOne(ctx, map[string]interface{}{"custom": ""}).Decode(&dstInstance)
	assert.Nil(t, err)
	assert.Equal(t, Nil, dstInstance.Custom)
}

func TestUUID_FromString(t *testing.T) {
	_, err := FromString("invalid-uuid")
	assert.NotNil(t, err)

	value, err := FromString("c50340f2-c2ff-4902-9c13-0728d880af9b")
	assert.Nil(t, err)
	assert.Equal(t, "c50340f2-c2ff-4902-9c13-0728d880af9b", value.String())
}

func TestFromStringOrNil(t *testing.T) {
	id := "ef969cc3-2a38-47b1-8f48-4b744ff99c29"

	assert.Equal(t, id, FromStringOrNil(id).String())
	assert.Equal(t, Nil, FromStringOrNil("invalid-uuid"))
}

func TestFromStringOrNilP(t *testing.T) {
	str := "65d08706-bf8b-4cef-8a0d-ea2cdfe4b311"

	assert.Equal(t, str, FromStringOrNilP(str).String())
	assert.Nil(t, FromStringOrNilP("invalid"))
}

func TestUUID_Validate(t *testing.T) {
	t.Run("should return error when uuid is invalid or nil", func(t *testing.T) {
		assert.NotNil(t, Nil.Validate())
		assert.NotNil(t, FromStringOrNil("2212312-3321"))
		assert.NotNil(t, FromStringOrNil(""))
	})

	t.Run("should not return error when uuid is valid", func(t *testing.T) {
		assert.Nil(t, New().Validate())
		assert.Nil(t, FromStringOrNil("23373280-054c-4941-bad6-7167274ab04d").Validate())
	})
}

func TestValidate(t *testing.T) {
	t.Run("should return error for invalid UUIDs", func(t *testing.T) {
		testCases := []interface{}{
			nil,
			Nil,
			"aaaa",
			1,
			[]byte(`00002-3212`),
		}

		for _, testCase := range testCases {
			assert.NotNil(t, Validate(testCase), "expected '%v' to be an invalid UUID", testCase)
		}
	})

	t.Run("should return nil for valid UUIDs", func(t *testing.T) {
		testCases := []interface{}{
			New(),
			FromStringOrNil("fffafd5d-4426-46ef-a811-1465b634d131"),
		}

		for _, testCase := range testCases {
			assert.Nil(t, Validate(testCase), "expected '%v' to be a valid UUID", testCase)
		}
	})
}

func TestUnmarshalJSON(t *testing.T) {
	type entity struct {
		ID UUID
	}

	t.Run("should unmarshal empty string to Nil UUID", func(t *testing.T) {
		str := `{"id": ""}`
		var entity entity

		assert.Nil(t, json.Unmarshal([]byte(str), &entity))
		assert.Equal(t, Nil.String(), entity.ID.String())
	})

	t.Run("should unmarshal null to Nil UUID", func(t *testing.T) {
		str := `{"id": null}`
		var entity entity

		assert.Nil(t, json.Unmarshal([]byte(str), &entity))
		assert.Equal(t, Nil.String(), entity.ID.String())
	})

	t.Run("should return error for invalid uuid", func(t *testing.T) {
		str := `{"id": "invalid"}`
		var entity entity

		assert.NotNil(t, json.Unmarshal([]byte(str), &entity))
	})

	t.Run("should unmarshal valid uuid string into valid UUID value", func(t *testing.T) {
		str := `{"id": "cc747ae6-dc75-4fc9-b581-ca1a7ec1ae94"}`
		var entity entity

		assert.Nil(t, json.Unmarshal([]byte(str), &entity))
		assert.Equal(t, "cc747ae6-dc75-4fc9-b581-ca1a7ec1ae94", entity.ID.String())
	})
}

func TestNewP(t *testing.T) {
	new := NewP()

	assert.NotNil(t, new.UUID)
	assert.NotEqual(t, Nil.String(), new.String())
}
