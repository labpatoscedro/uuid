package uuid

import (
	"errors"
	"fmt"
	"strings"

	uuid "github.com/satori/go.uuid"
	"go.mongodb.org/mongo-driver/bson/bsontype"
	"go.mongodb.org/mongo-driver/x/bsonx/bsoncore"
)

type UUID struct {
	uuid.UUID
}

func New() UUID {
	return UUID{uuid.NewV4()}
}

func NewP() *UUID {
	id := New()

	return &id
}

var ErrInvalidLength = errors.New("invalid UUID length")

func (u *UUID) UnmarshalJSON(bytes []byte) error {
	str := strings.Trim(string(bytes), `"`)

	if str == "" || str == `null` {
		u.UUID = Nil.UUID

		return nil
	}

	return u.UUID.UnmarshalText([]byte(str))
}

func (u *UUID) MarshalBSONValue() (bsontype.Type, []byte, error) {
	return bsontype.Binary, bsoncore.AppendBinary(nil, 4, u.UUID[:]), nil
}

func (u *UUID) UnmarshalBSONValue(t bsontype.Type, raw []byte) error {
	switch t {
	case bsontype.Binary:
		return u.unmarshalBinaryUUID(raw)
	case bsontype.String:
		return u.unmarshalStringUUID(raw)
	case bsontype.Null:
		return nil
	default:
		return errors.New("invalid format on unmarshall bson value")
	}
}

func (u *UUID) unmarshalBinaryUUID(raw []byte) error {
	_, data, _, ok := bsoncore.ReadBinary(raw)
	if !ok {
		return errors.New("not enough bytes to unmarshal bson value")
	}

	copy(u.UUID[:], data)
	return nil
}

func (u *UUID) unmarshalStringUUID(raw []byte) error {
	str, _, _ := bsoncore.ReadString(raw)
	if str == "" {
		return nil
	}

	id, err := FromString(str)
	if err != nil {
		return err
	}

	copy(u.UUID[:], id.Bytes())
	return nil
}

func (u UUID) Validate() error {
	if u == Nil {
		return errors.New("invalid or missing uuid value")
	}

	return nil
}

func FromString(s string) (UUID, error) {
	id, err := uuid.FromString(s)
	return UUID{id}, err
}

func FromStringOrNil(s string) UUID {
	return UUID{uuid.FromStringOrNil(s)}
}

func FromStringOrNilP(s string) *UUID {
	id := FromStringOrNil(s)
	if id == Nil {
		return nil
	}

	return &id
}

func Validate(value interface{}) error {
	if v, ok := value.(UUID); !ok || v == Nil {
		return fmt.Errorf("invalid UUID: %v", value)
	}

	return nil
}
